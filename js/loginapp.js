var app = angular.module('HIS', ['ngRoute',

  'kendo.directives' ,
  'ngCookies',
  'ngResource',
  'HIS.Login'
  
 


  ])
app.run(['$rootScope',  '$location', '$window', function($rootScope, $location, $window) {
  $window.ga('create', 'UA-76803209-1', 'auto');
  $rootScope.$on('$stateChangeSuccess', function (event) {
    $window.ga('send', 'pageview', $location.path());
  });


}]);

app.config(['$routeProvider',function($routeProvider) {

    $routeProvider
        .when('/', {
            templateUrl : 'js/app/Login/loginView.html',
            controller  : 'loginController'
        })
         .when('/recuperarClave', {
            templateUrl : 'js/app/Login/recuperarPasswordView.html',
            controller  : 'recupPassController'
        })
        .when('/recuperarClave/nuevaContrasenia/:idUsuario', {
            templateUrl : 'js/app/Login/cambiarPassView.html',
            controller  : 'nuevoPasswordController'
        })
}])
