var app = angular.module('HIS', ['ngRoute',


  'kendo.directives',
  'ngCookies',

  'angucomplete-alt',
  'checklist-model',
  'ngResource',
  'HIS.UserAdmin',
  'globalErrors',
  'ui.bootstrap',
  'HIS.MainInicial',
  'HIS.Shared',
  'HIS.Profile',
  'HIS.Despacho',
  'HIS.Pedidos',
  'HIS.Productos',
  'HIS.Clientes'


])
/*
app.run(['$rootScope','$cookieStore', '$location', '$window', 'ProfileService',  function($rootScope, $cookieStore, $location, $window,ProfileService) {
  //$window.ga('create', 'UA-76803209-1', 'auto');
  $rootScope.$on('$routeChangeSuccess', function (event) {
    //$window.ga('send', 'pageview', $location.path());
  });

  if (localStorage.getItem("sJWT") === null){

    if ($cookieStore.get('tokenActual') === null  || !$cookieStore.get('tokenActual')  ){
        window.location= "index.html";
    }else{
        localStorage.setItem('sJWT', $cookieStore.get('tokenActual')); 

    }

  }else{
    if (localStorage.getItem("Parametros") === null){
        ProfileService.Parametros().get(function(objParametros) {
          for(i=0;i<objParametros.length;i++){
            localStorage.setItem(objParametros[i].nombreParam,JSON.stringify(objParametros[i].valorParam) );
          }
        })
    }
  }

}]);
*/
app.config(['$routeProvider', function ($routeProvider) {

  EvaluarAcceso = function (valor) {

    if (localStorage.getItem("funcionalidades") != null) {
      var funcionPermitida = false;
      var paramFuncionalidades = JSON.parse(localStorage.getItem('funcionalidades'));

      for (i = 0; i < paramFuncionalidades.length; i++) {
        if (paramFuncionalidades[i] == valor) {

          $('#drawerExample').addClass('folded').removeClass('open').attr('aria-expanded', false)

          funcionPermitida = true;
          break;
        }
      }

      if (!funcionPermitida) {
        window.location = "/";
      }

    }

  };

  $routeProvider

    .when('/main/chart', {
      templateUrl: 'js/app/Main/mainChart/mainInicialView.html',
      controller: 'mainInicialController'
      // resolve:{              
      //      "check": function(){ EvaluarAcceso('Flujo de Caja')}                                  
      //  }

    })


    .when('/usuario/administrarUsuarios', {
      templateUrl: 'js/app/UserAdmin/administracionUsuarioView.html',
      controller: 'administracionUsuariosController',
      // resolve:{              
      //      "check": function(){ EvaluarAcceso('Flujo de Caja')}                                  
      //  }

    })


    .when('/cambiarContrasenia', {
      templateUrl: 'js/app/Profile/cambiarPasswordView.html',
      controller: 'ProfileCambiarPassController',
      resolve: {
        "check": function () { EvaluarAcceso('Cambiar Contraseña') }

      }

    })

    .when('/productos', {
      templateUrl: 'js/app/Productos/productosView.html',
      controller: 'ProductosController',
      resolve: {
        "check": function () { EvaluarAcceso('Productos') }
      }
    })

    .when('/partidas', {
      templateUrl: 'js/app/Productos/partidasView.html',
      controller: 'PartidasController',
      resolve: {
        "check": function () { EvaluarAcceso('Productos') }
      }
    })
    .when('/pedidos', {
      templateUrl: 'js/app/Pedidos/pedidosView.html',
      controller: 'PedidosController',
      resolve: {
        "check": function () { EvaluarAcceso('Pedidos') }

      }

    })

    .when('/clientes', {
      templateUrl: 'js/app/Cliente/clientesView.html',
      controller: 'ClientesController',
      resolve: {
        "check": function () { EvaluarAcceso('Clientes') }

      }

    })

    .when('/despachos', {
      templateUrl: 'js/app/Despacho/despachoView.html',
      controller: 'despachoController',
      resolve: {
        "check": function () { EvaluarAcceso('Despachos') }

      }

    })

    .when('/ranking', {
      templateUrl: 'js/app/Pedidos/rankingView.html',
      controller: 'RankingController',
      resolve: {
        "check": function () { EvaluarAcceso('Pedidos') }

      }

    })
     .when('/confirmaciondesp', {
      templateUrl: 'js/app/Despacho/confirmacionView.html',
      controller: 'confdespController',
      resolve: {
        "check": function () { EvaluarAcceso('Despachos') }

      }

    })
     .when('/listaprecios', {
      templateUrl: 'js/app/Productos/listapreciosView.html',
      controller: 'listaPreciosController',
      resolve: {
        "check": function () { EvaluarAcceso('Productos') }

      }

    })
    .when('/despachos/:id_despacho', {
      templateUrl: 'js/app/Despacho/despachoDetalleView.html',
      controller: 'despachoDetalleController',
      resolve: {
        "check": function () { EvaluarAcceso('Despachos') }

      }

    })


    .otherwise({
      redirectTo: '/main/chart'
    });
}])