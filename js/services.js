angular.module('starter.services', ['ngCookies'])

.factory('connectionString',function(){
  //var serviciosBase="http://holinsys.net:8015/api/";
  //var serviciosBase="http://192.168.5.26:8015/api/";
  //var serviciosBase="http://192.168.0.7:3755/api/";
  var serviciosBase="http://localhost:8080";
 // var serviciosBase="http://olimpo.holinsys.net:8501/";
  
  return{
    urlBase:function(){
      return serviciosBase;
    }
  }
})




.service('upload', ["$http", "$q",function ($http, $q) 
{
  this.uploadFile = function(file, name)
  {
    var deferred = $q.defer();
    var formData = new FormData();
    formData.append("name", name);
    formData.append("file", file);
    return $http.post("server.php", formData, {
      headers: {
        "Content-type": undefined
      },
      transformRequest: angular.identity
    })
    .success(function(res)
    {
      deferred.resolve(res);
    })
    .error(function(msg, code)
    {
      deferred.reject(msg);
    })
    return deferred.promise;
  } 
}])





        
};
}])

