HISProfile.factory('ClienteService', ['$resource', '$q', '$http', 'connStringSVC', '$cookieStore', function ($resource, $q, $http, connStringSVC, $cookieStore) {

    return {
        clientes: function () {
            return $resource(connStringSVC.urlBase() + '/Cliente/:id', { id: '@_id' }, {
                update: { method: 'PUT', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } }
            });
        },
         Listas: function () {
            return $resource(connStringSVC.urlBase() + '/ListaPrecios/:id', { id: '@_id' }, {
                save: { method: 'POST', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                query: { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } }
            });
        },
    }

}])