HISProfile.controller('ClientesController', ['$scope', '$rootScope', 'PedidoService', 'ClienteService',
            function ($scope, $rootScope, PedidoService, ClienteService) {

	function ini() {

        // tabla de pedidos
        $scope.regs;
        $scope.mostrarFiltros = false;
        $scope.registrations = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    options.success($scope.regs);
                }
            },
            schema: {
                model: {
                    fields: {
                        id: { type: "number", editable: false },
                        nombres: { type: "string", editable: false },
                        nombreCorto: { type: "string", editable: false },
                        RUC: { type: "string", editable: false },
                        direccionFiscal: { type: "string", editable: false },
                        direccionDespacho: { type: "string", editable: false },
                        requiereAprobacion: { type: "number", editable: false },
                        idLista: { type: "number", editable: false }
                    }
                }
            },
            pageSize: 20,
            Paging: true
        });
        $scope.gridColumns = [

            {
                field: "nombres",
                title: "Razon Social"
            },

            {
                field: "nombreCorto",
                title: "Nombre Corto"
            },
            {
                field: "RUC",
                title: "RUC"
            },
            {
                field: "direccionFiscal",
                title: "Dirección"
            },
            {
                width: 100,
                attributes: { style: "text-align:center;" },
                command: [
                	{ template: '<kendo-button sprite-css-class="\'k-icon k-i-pencil\'" ng-click="showData(dataItem)"></kendo-button>' },
        			{ template: '<img src="/img/error.png" ng-click="eliminarCliente(dataItem)" style="margin-left: 5px;"/>' }
                ]
            }
        ];


        ClienteService.Listas().query(function (lista) {
                $scope.listas = lista
        })

        $scope.gridOptions = {
            height: "70vh",
            excel: {
                fileName: "Clientes.xlsx",
                allPages: true,
                filterable: false,
            },
            filterable: {
                mode: "row",
                operators: {
                    date: {
                        eq: "Igual a",
                        neq: "Diferente a",
                        gte: "Mayor o igual a",
                        gt: "Mayor a",
                        lte: "Menor o igual a",
                        lt: "Menor a"
                    },
                    number: {
                        eq: "Igual a",
                        neq: "Diferente a",
                        gte: "Mayor o igual a",
                        gt: "Mayor a",
                        lte: "Menor o igual a",
                        lt: "Menor a"
                    },
                    string: {
                        startswith: "Inicia con",
                        endswith: "Termina con",
                        eq: "Es igual a",
                        neq: "Es diferente a",
                        contains: "Contiene",
                        doesnotcontain: "No Contiene"
                    }
                },
                messages: {
                    and: "Y",
                    or: "O",
                    filter: "Filtrar",
                    clear: "Limpiar",
                    info: "Mostrar los valores que sean"
                }
            },
            sortable: true,
            reorderable: true,
            resizable: true,
            editable: true,
            columnMenu: {
                messages: {
                    sortAscending: "Ordenar Ascendentemente",
                    sortDescending: "Ordenar Descendentemente",
                    columns: "Columnas",
                    filter: "Filtro Especial",
                }
            },
            pageable: {
                messages: {
                    display: "{0} - {1} de {2} Registros",
                    empty: "No existen datos",
                    page: "Página",
                    of: "de {0}",
                    itemsPerPage: "Páginas",
                    first: "Primero",
                    previous: "Anterior",
                    next: "Siguiente",
                    last: "Último",
                    refresh: "Refrescar"
                }
            }, dataBound: function (e) {
                $("#loading").fadeOut(200);
                setTimeout(
                    function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    }
                    , 100)
            }
        }

        $scope.regs = PedidoService.Pedidos().clientes(function (clientes) {
            $scope.registrations.read();
        })

    }

    ini()

    $scope.showData = function(dataItem){
        $scope.currentClient = dataItem;
        $scope.currentClient = {
        	id: dataItem.id,
            nombres: dataItem.nombres,
            nombreCorto: dataItem.nombreCorto,
            RUC: dataItem.RUC,
            direccionFiscal: dataItem.direccionFiscal,
            direccionDespacho: dataItem.direccionDespacho,
           
            diasPartida: dataItem.diasPartida,
            requiereAprobacion: (dataItem.requiereAprobacion == 1 ? true : false)
        }
        if ( dataItem.idLista){
            $scope.currentClient.idLista = dataItem.idLista.toString();
        }
        $("#popupDetailClient").modal('toggle')
    }

    $scope.editUser= function(isValid){

		$scope.submitted = true
		if(isValid){
			var data = {
				nombreCorto: $scope.currentClient.nombreCorto,
				direccionDespacho: $scope.currentClient.direccionDespacho,
				requiereAprobacion: ($scope.currentClient.requiereAprobacion ? 1 : 0),
                diasPartida: $scope.currentClient.diasPartida,
                idLista: $scope.currentClient.idLista

			}
			ClienteService.clientes().update( {id:$scope.currentClient.id}, $scope.currentClient, function() {
              	window.location.reload();
				//$('#popupDetailClient').modal('hide');
			},function (error){
				
			});
		}

	};

}])
