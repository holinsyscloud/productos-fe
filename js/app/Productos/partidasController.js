HISProductos.controller('PartidasController', ['$scope', '$rootScope', 'ProductoService', '$window', 'connStringSVC','ClienteService',
    function ($scope, $rootScope, ProductoService, $window, connStringSVC,ClienteService) {

        function ini() {

            // tabla de pedidos
            $scope.regs;
            $scope.mostrarFiltros = false;
            $scope.registrations = new kendo.data.DataSource({
                transport: {
                    read: function (options) {
                        options.success($scope.regs);
                    }
                },
                schema: {
                    model: {
                        fields: {
                            id: { type: "number", editable: false },
                            partida: { type: "string", editable: false },
                            fechaIngreso: { type: "date", editable: false },
                            extra: { type: "number", editable: false },
                           
                            
                        }
                    }
                },
                pageSize: 20,
                Paging: true
            });
            $scope.gridColumns = [
                {
                    field: "partida",
                    title: "Partida"
                },
               
                {
                    field: "fechaIngreso",
                    title: "Fecha",
                    type: "date",
                    format: "{0:dd/MM/yyyy}",
                },
               
                
                {
                    field: "extra",
                    title: "Precio Extra (S/.)",
                    attributes: { style: "text-align:right;" },
                    width: 190
                },
                 {
                    title: "",
                    width: 100,
                    attributes: { style: "text-align:center;" },
                    command: [{ text: " ", template: '<kendo-button sprite-css-class="\'k-icon k-i-pencil\'" ng-click="showOrder(dataItem)"></kendo-button>' }]
                },
            ];
            $scope.gridOptions = {
                height: "70vh",
                excel: {
                    fileName: "Partidas.xlsx",
                    allPages: true,
                    filterable: false,
                },
                filterable: {
                    mode: "row",
                    operators: {
                        date: {
                            eq: "Igual a",
                            neq: "Diferente a",
                            gte: "Mayor o igual a",
                            gt: "Mayor a",
                            lte: "Menor o igual a",
                            lt: "Menor a"
                        },
                        number: {
                            eq: "Igual a",
                            neq: "Diferente a",
                            gte: "Mayor o igual a",
                            gt: "Mayor a",
                            lte: "Menor o igual a",
                            lt: "Menor a"
                        },
                        string: {
                            startswith: "Inicia con",
                            endswith: "Termina con",
                            eq: "Es igual a",
                            neq: "Es diferente a",
                            contains: "Contiene",
                            doesnotcontain: "No Contiene"
                        }
                    },
                    messages: {
                        and: "Y",
                        or: "O",
                        filter: "Filtrar",
                        clear: "Limpiar",
                        info: "Mostrar los valores que sean"
                    }
                },
                sortable: true,
                reorderable: true,
                resizable: true,
                editable: true,
                columnMenu: {
                    messages: {
                        sortAscending: "Ordenar Ascendentemente",
                        sortDescending: "Ordenar Descendentemente",
                        columns: "Columnas",
                        filter: "Filtro Especial",
                    }
                },
                pageable: {
                    messages: {
                        display: "{0} - {1} de {2} Registros",
                        empty: "No existen datos",
                        page: "Página",
                        of: "de {0}",
                        itemsPerPage: "Páginas",
                        first: "Primero",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Último",
                        refresh: "Refrescar"
                    }
                }, dataBound: function (e) {
                    $("#loading").fadeOut(200);
                    setTimeout(
                        function () {
                            $('[data-toggle="tooltip"]').tooltip();
                        }
                        , 100)
                }
            }
           

             $scope.regs = ProductoService.Partidas().query( function (lista_pedidos) {
                $scope.registrations.read();
            })


        }
        ini()

          $scope.editPartida = {};
      
         $scope.showOrder = function(dataItem){
            $scope.editPartida = {};
            $scope.editPartida.id  = dataItem.id;
            $scope.editPartida.partida  = dataItem.partida;

            $scope.editPartida.extra  = dataItem.extra;
            $("#popupDetailOrder").modal('toggle')
        }

        $scope.responder_pedido = function (id_pedido, respuesta, razonRechazo) {

        PedidoService.Pedidos().responder_pedido({ "id": id_pedido }, { "aprobado": respuesta, "razonRechazo": razonRechazo}, function (estado_pedidos) {
            $scope.buscar_pedidos();
        })

    }

        $scope.editarProducto = function (producto) {
            $scope.productoEditar = producto;
        }


        $scope.submitForm = function (isValid) {
            $scope.submitted = true
            if (isValid) {

               
                ProductoService.Partidas().update({id: $scope.editPartida.id}, $scope.editPartida, function (despacho_creado) {
                      $("#popupDetailOrder").modal('toggle')
                      $scope.regs = ProductoService.Partidas().query( function (lista_pedidos) {
                        $scope.registrations.read();

                    })
                })

            }
        }
       
       

        

    }])