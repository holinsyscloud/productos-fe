HISProfile.factory('ProductoService', ['$resource', '$q', '$http', 'connStringSVC', '$cookieStore', function ($resource, $q, $http, connStringSVC, $cookieStore) {


    return {
        ProductosTodos: function () {
            return $resource(connStringSVC.urlBase() + '/ProductosTodos/:id', { id: '@_id' }, {
                queryXLista: { url: connStringSVC.urlBase() + '/ProductosTodosLista/:id', isArray: true, method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                query: { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                save: { method: 'POST', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                articulos: { url: connStringSVC.urlBase() + '/ArticulosTodos/', isArray: true, method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                fibras: { url: connStringSVC.urlBase() + '/FibrasTodos/', isArray: true, method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                titulos: { url: connStringSVC.urlBase() + '/TitulosTodos/', isArray: true, method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                especificaciones: { url: connStringSVC.urlBase() + '/EspecificacionesTodos/', isArray: true, method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                gamas: { url: connStringSVC.urlBase() + '/GamasTodos/', isArray: true, method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                colores: { url: connStringSVC.urlBase() + '/ColoresTodos/', isArray: true, method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                acabados: { url: connStringSVC.urlBase() + '/AcabadosTodos/', isArray: true, method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                actualizar_productos_excel: { url: connStringSVC.urlBase() + '/ProductosArmados/:id', isArray: false, method: 'PUT', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                actualizar_stocks: { url: connStringSVC.urlBase() + '/ProductosStocks/', isArray: false, method: 'POST', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } }
            });
        },
        Partidas: function () {
            return $resource(connStringSVC.urlBase() + '/PartidasUp/:id', { id: '@_id' }, {
                query: { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                update: { method: 'PUT', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
             });
        }
    }

}])