HISProductos.controller('listaPreciosController', ['$scope', '$rootScope', 'ClienteService', '$window', 'connStringSVC',
    function ($scope, $rootScope, ClienteService, $window, connStringSVC) {

        function ini() {

            // tabla de pedidos
            $scope.regs;
            $scope.mostrarFiltros = false;
            $scope.registrations = new kendo.data.DataSource({
                transport: {
                    read: function (options) {
                        options.success($scope.regs);
                    }
                },
                schema: {
                    model: {
                        fields: {
                           
                            nombre: { type: "string", editable: false },
                           
                        }
                    }
                },
                pageSize: 20,
                Paging: true
            });
            $scope.gridColumns = [
                {
                    field: "nombre",
                    title: "Nombre"
                },
           
            ];
            $scope.gridOptions = {
                height: "70vh",
                
                filterable: {
                    mode: "row",
                    operators: {
                        date: {
                            eq: "Igual a",
                            neq: "Diferente a",
                            gte: "Mayor o igual a",
                            gt: "Mayor a",
                            lte: "Menor o igual a",
                            lt: "Menor a"
                        },
                        number: {
                            eq: "Igual a",
                            neq: "Diferente a",
                            gte: "Mayor o igual a",
                            gt: "Mayor a",
                            lte: "Menor o igual a",
                            lt: "Menor a"
                        },
                        string: {
                            startswith: "Inicia con",
                            endswith: "Termina con",
                            eq: "Es igual a",
                            neq: "Es diferente a",
                            contains: "Contiene",
                            doesnotcontain: "No Contiene"
                        }
                    },
                    messages: {
                        and: "Y",
                        or: "O",
                        filter: "Filtrar",
                        clear: "Limpiar",
                        info: "Mostrar los valores que sean"
                    }
                },
                sortable: true,
                reorderable: true,
                resizable: true,
                editable: true,
                columnMenu: {
                    messages: {
                        sortAscending: "Ordenar Ascendentemente",
                        sortDescending: "Ordenar Descendentemente",
                        columns: "Columnas",
                        filter: "Filtro Especial",
                    }
                },
                pageable: {
                    messages: {
                        display: "{0} - {1} de {2} Registros",
                        empty: "No existen datos",
                        page: "Página",
                        of: "de {0}",
                        itemsPerPage: "Páginas",
                        first: "Primero",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Último",
                        refresh: "Refrescar"
                    }
                }, dataBound: function (e) {
                    $("#loading").fadeOut(200);
                    setTimeout(
                        function () {
                            $('[data-toggle="tooltip"]').tooltip();
                        }
                        , 100)
                }
            }
           

           
        }
        ini()

       

        $scope.buscar_listas = function () {
            $scope.regs = ClienteService.Listas().query({}, function (lista_pedidos) {
                $scope.registrations.read();
            })
        }
         $scope.buscar_listas();
        $scope.crearProducto = function () {
            $scope.productoCrear = {};
        }

      

        $scope.submitForm = function (isValid) {
            $scope.submitted = true
            if (isValid) {

                $scope.productoCrear.nombrelista = $scope.listaCrear.nombrelista;
               
                ClienteService.Listas().save($scope.productoCrear, function (despacho_creado) {
                      $('#popupNuevoProducto').modal('hide');
                      $scope.buscar_listas();

                })

            }
        }
      

    }])