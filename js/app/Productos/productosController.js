HISProductos.controller('ProductosController', ['$scope', '$rootScope', 'ProductoService', '$window', 'connStringSVC','ClienteService',
    function ($scope, $rootScope, ProductoService, $window, connStringSVC,ClienteService) {

        function ini() {

            // tabla de pedidos
            $scope.regs;
            $scope.mostrarFiltros = false;
            $scope.registrations = new kendo.data.DataSource({
                transport: {
                    read: function (options) {
                        options.success($scope.regs);
                    }
                },
                schema: {
                    model: {
                        fields: {
                            id: { type: "number", editable: false },
                            nombre: { type: "string", editable: false },
                            color: { type: "string", editable: false },
                            gama: { type: "string", editable: false },
                            stock: { type: "number", editable: false },
                            precioMinimo: { type: "number", editable: false },
                            codigo: { type: "string", editable: false }
                        }
                    }
                },
                pageSize: 20,
                Paging: true
            });
            $scope.gridColumns = [
                {
                    field: "nombre",
                    title: "Nombre"
                },

                {
                    field: "color",
                    title: "Color"
                },
                 {
                    field: "gama",
                    title: "Gama",
                    width: 200
                },
                {
                    field: "stock",
                    title: "Stock Actual (Kg)",
                    attributes: { style: "text-align:right;" },
                    width: 170
                },
                {
                    field: "precioMinimo",
                    title: "Precio Mínimo (S/.)",
                    attributes: { style: "text-align:right;" },
                    width: 190
                },
                {
                    field: "codigo",
                    title: "Código"
                }
            ];
            $scope.gridOptions = {
                height: "70vh",
                excel: {
                    fileName: "Productos.xlsx",
                    allPages: true,
                    filterable: false,
                },
                filterable: {
                    mode: "row",
                    operators: {
                        date: {
                            eq: "Igual a",
                            neq: "Diferente a",
                            gte: "Mayor o igual a",
                            gt: "Mayor a",
                            lte: "Menor o igual a",
                            lt: "Menor a"
                        },
                        number: {
                            eq: "Igual a",
                            neq: "Diferente a",
                            gte: "Mayor o igual a",
                            gt: "Mayor a",
                            lte: "Menor o igual a",
                            lt: "Menor a"
                        },
                        string: {
                            startswith: "Inicia con",
                            endswith: "Termina con",
                            eq: "Es igual a",
                            neq: "Es diferente a",
                            contains: "Contiene",
                            doesnotcontain: "No Contiene"
                        }
                    },
                    messages: {
                        and: "Y",
                        or: "O",
                        filter: "Filtrar",
                        clear: "Limpiar",
                        info: "Mostrar los valores que sean"
                    }
                },
                sortable: true,
                reorderable: true,
                resizable: true,
                editable: true,
                columnMenu: {
                    messages: {
                        sortAscending: "Ordenar Ascendentemente",
                        sortDescending: "Ordenar Descendentemente",
                        columns: "Columnas",
                        filter: "Filtro Especial",
                    }
                },
                pageable: {
                    messages: {
                        display: "{0} - {1} de {2} Registros",
                        empty: "No existen datos",
                        page: "Página",
                        of: "de {0}",
                        itemsPerPage: "Páginas",
                        first: "Primero",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Último",
                        refresh: "Refrescar"
                    }
                }, dataBound: function (e) {
                    $("#loading").fadeOut(200);
                    setTimeout(
                        function () {
                            $('[data-toggle="tooltip"]').tooltip();
                        }
                        , 100)
                }
            }
             ClienteService.Listas().query(function (lista) {
                    $scope.listas = lista
            })

            ProductoService.ProductosTodos().articulos(function (articulos) {
                $scope.articulos = articulos
            })
            ProductoService.ProductosTodos().fibras(function (fibras) {
                $scope.fibras = fibras
            })
            ProductoService.ProductosTodos().titulos(function (titulos) {
                $scope.titulos = titulos
            })
            ProductoService.ProductosTodos().especificaciones(function (especificaciones) {
                $scope.especificaciones = especificaciones
                ProductoService.ProductosTodos().acabados(function (acabados) {
                    $scope.acabados = acabados
                })
            })
            ProductoService.ProductosTodos().gamas(function (gamas) {
                $scope.gamas = gamas
                ProductoService.ProductosTodos().colores(function (colores) {
                    $scope.colores = colores
                })
            })


        }
        ini()

        tabla_errores_carga_excel = function () {
            // tabla de pedidos
            $scope.regs_excel;
            $scope.mostrarFiltros = false;
            $scope.registrations_excel = new kendo.data.DataSource({
                transport: {
                    read: function (options) {
                        options.success($scope.regs_excel);
                    }
                },
                schema: {
                    model: {
                        fields: {
                            id: { type: "number", editable: false },
                            nombre: { type: "string", editable: false },
                            stock_actual: { type: "number", editable: false },
                            precio_minimo: { type: "number", editable: false }
                        }
                    }
                },
                pageSize: 20,
                Paging: true
            });
            $scope.gridColumns_excel = [

                {
                    field: "nombre",
                    title: "Nombre"
                },
                {
                    field: "stock_actual",
                    title: "Stock Actual (Kg)",
                    attributes: { style: "text-align:right;" },
                    width: 180
                },
                {
                    field: "precio_minimo",
                    title: "Precio Mínimo (S/.)",
                    attributes: { style: "text-align:center;" },
                    width: 200
                }
            ];
            $scope.gridOptions_excel = {
                height: "70vh",
                excel: {
                    fileName: "Productos.xlsx",
                    allPages: true,
                    filterable: false,
                },
                pageable: {
                    messages: {
                        display: "{0} - {1} de {2} Registros",
                        empty: "No existen datos",
                        page: "Página",
                        of: "de {0}",
                        itemsPerPage: "Páginas",
                        first: "Primero",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Último",
                        refresh: "Refrescar"
                    }
                }
            }
        }

        $scope.buscar_productos = function () {
            $scope.regs = ProductoService.ProductosTodos().query({}, function (lista_pedidos) {
                $scope.registrations.read();
            })
        }

        $scope.crearProducto = function () {
            $scope.productoCrear = {};
        }

        $scope.editarProducto = function (producto) {
            $scope.productoEditar = producto;
        }

        $scope.buscar_productos();

        $scope.submitForm = function (isValid) {
            $scope.submitted = true
            if (isValid) {

                $scope.productoCrear.articulo = $('#selectCrearArticulo option:selected').text();
                $scope.productoCrear.fibra = $('#selectCrearFibra option:selected').text();
                $scope.productoCrear.titulo = $('#selectCrearTitulo option:selected').text();
                $scope.productoCrear.especificacion = $('#selectCrearEspecificacion option:selected').text();
                $scope.productoCrear.gama = $('#selectCrearGama option:selected').text();
                $scope.productoCrear.color = $('#selectCrearColor option:selected').text();
                $scope.productoCrear.acabado = $('#selectCrearAcabado option:selected').text();
                ProductoService.ProductosTodos().save($scope.productoCrear, function (despacho_creado) {
                    window.location.reload();
                })

            }
        }
        $scope.filtrarporLista = function(){
            $scope.regs = ProductoService.ProductosTodos().queryXLista({id:$scope.listaPicked}, function (lista_pedidos) {
                $scope.registrations.read();
            })


        }
        $scope.leerArchivoStock = function(files){

            if (files && files.length == 1) {

                $scope.cargandoArchivo = true;
                var f = files[0];
                var reader = new FileReader();
                var name = f.name;

                $scope.partidas = [];
                var fila = 5;
                reader.onload = function (e) {

                    $("#fileStock").val(null);
                    try {

                        var data = e.target.result;
                        var workbook = XLSX.read(data, { type: 'binary' });
                        var worksheet = workbook.Sheets['Stocks'];

                        var address_of_cell = 'B' + fila;
                        var numPartida = worksheet[address_of_cell].v;
                        var partida = {}

                        while(numPartida){
                            partida = {
                                partida: numPartida.toString().trim(),
                                fechaIngreso: worksheet['C' + fila].v,
                                stockKg: worksheet['E' + fila].v,
                                producto: worksheet['F' + fila].v.trim(),
                                gama: worksheet['G' + fila].v.trim(),
                                color: worksheet['H' + fila].v.trim()
                            }
                            if (worksheet['I' + fila] && worksheet['I' + fila].v != 'ITP'){
                                partida.cliente = worksheet['I' + fila].v.toString().trim()
                            }else{
                                partida.cliente = ''
                            }
                            if (worksheet['J' + fila]){
                                partida.ubicacion = worksheet['J' + fila].v.toString().trim()
                            }else{
                                partida.ubicacion = ''
                            }
                            $scope.partidas.push(partida);

                            fila = fila + 1;
                            address_of_cell = 'B' + fila;
                            if (worksheet[address_of_cell]){
                                numPartida = worksheet[address_of_cell].v;
                            }else{
                                numPartida = false;
                            }
                        }

                        if ($scope.partidas.length > 0){
                            // Envio de data al servidor
                            ProductoService.ProductosTodos().actualizar_stocks($scope.partidas, function (productos_error) {
                                if (productos_error.length > 0) {
                                    $('#popupErroresCarga').modal('show');
                                    $scope.regs_excel = productos_error
                                    $scope.registrations_excel.read();
                                }else {
                                    window.location.reload();
                                }
                            })
                        }

                    } catch (err) {
                        console.log(err)
                        console.log(fila)
                        $scope.errorCargarArchivo = true;
                    }
                    
                };

                reader.readAsBinaryString(f);
                $scope.cargandoArchivo = false;
            }
        }

        $scope.leerArchivoCosto = function (files) {

            if (files && files.length == 1) {
                $scope.cargandoArchivo = true;
                var f = files[0];
                var reader = new FileReader();
                var name = f.name;
                $scope.articulosPorRegistrar = [];
                $scope.articulosPorAgregar = [];

                reader.onload = function (e) {

                    $("#fileCosts").val(null);
                    try {
                        var data = e.target.result;
                        var workbook = XLSX.read(data, { type: 'binary' });
                        var worksheet = workbook.Sheets['Precios'];

                        var fila = 5;
                        var address_of_cell = 'B' + fila;
                        var nombre = worksheet[address_of_cell].v;
                        
                        var encontrado = false;
                        var agregados = 0;
                        var creados = 0;

                        while (nombre) {

                            var articuloPorCrear = {
                                nombre: nombre.toString().trim(),
                                gama: worksheet['D' + fila].v,
                                color: worksheet['C' + fila].v,
                                precio_minimo: parseFloat((worksheet['E' + fila].v))
                            }

                            $scope.articulosPorRegistrar.push(articuloPorCrear);

                            fila = fila + 1;
                            address_of_cell = 'B' + fila;
                            if (worksheet[address_of_cell])
                                nombre = worksheet[address_of_cell].v;
                            else
                                nombre = false;
                        }

                        if ($scope.articulosPorRegistrar && $scope.articulosPorRegistrar.length > 0) {
                            $scope.mostrarArticulosPorRegistrar();
                        }
                    } catch (err) {
                        console.log(err)
                        console.log(fila)

                        $scope.errorCargarArchivo = true;
                    }
                };
                reader.readAsBinaryString(f);
                $scope.cargandoArchivo = false;
            }
        }

        $scope.mostrarArticulosPorRegistrar = function () {
            tabla_errores_carga_excel()
            ProductoService.ProductosTodos().actualizar_productos_excel({ "id": $scope.listaPicked }, $scope.articulosPorRegistrar, function (productos_error) {

                $scope.regs = ProductoService.ProductosTodos().queryXLista({id:$scope.listaPicked}, function (lista_pedidos) {
                    $scope.registrations.read();
                })
                if (productos_error.length > 0) {
                    $('#popupErroresCarga').modal('show');
                    $scope.regs_excel = productos_error
                    $scope.registrations_excel.read();
                }
                else {
                    $scope.mostrar_carga_completa = true
                    setTimeout(function () {
                        $scope.mostrar_carga_completa = false
                    }, 2000);
                }
            })
        }

        $scope.exportarStocks = function () {
            var rutaServicio = "/StockExport"
            var params = {
                header: Base64.encode(localStorage.getItem('sJWT'))
            };
            window.open([connStringSVC.urlBase() + rutaServicio, $.param(params)].join('?'));
        }

        $scope.exportarPrecios = function () {
            var rutaServicio = "/PrecioExport"
            var params = {
                list: $scope.listaPicked,
                header: Base64.encode(localStorage.getItem('sJWT'))
            };
            window.open([connStringSVC.urlBase() + rutaServicio, $.param(params)].join('?'));
        }

    }])