HISProfile.controller('despachoDetalleController', ['$scope', '$rootScope', 'DespachoService', 'PedidoService', '$routeParams', 'connStringSVC', function ($scope, $rootScope, DespachoService, PedidoService, $routeParams, connStringSVC) {

    function ini() {
        $scope.id_despacho = $routeParams.id_despacho;
        // tabla de pedidos
        $scope.regs;
        $scope.mostrarFiltros = false;
        $scope.registrations = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    options.success($scope.regs);
                }
            },
            schema: {
                model: {
                    fields: {
                        cliente: { type: "string", editable: false },
                        RUC: { type: "string", editable: false },
                        nombreProducto: { type: "string", editable: false },
                        cantidad: { type: "number", editable: false },
                        cantidadDespachada: { type: "number", editable: true },
                        direccionDespacho: { type: "string", editable: true },
                        partida: { type: "string", editable: false }

                    }
                },

            },
            pageSize: 20,
            Paging: true,
        });
        $scope.registrationsColumns = [
            {
                field: "cliente",
                title: "Cliente"
            },
            {
                field: "RUC",
                title: "RUC"
            },
            {
                field: "partida",
                title: "Partida"
            },
            {
                field: "nombreProducto",
                title: "Producto"
            },
            {
                field: "cantidad",
                title: "Cantidad (Kg)"
            },
            {
                field: "cantidadDespachada",
                title: "Cantidad Despachada (Kg)"
            },
            {
                field: "direccionDespacho",
                title: "Dirección",

            },
        ];
        $scope.gridOptions = {
            excel: {
                fileName: "Despachos.xlsx",
                allPages: true,
                filterable: true,
            },
            height: "70vh",
            filterable: {
                mode: "row",
                operators: {
                    date: {
                        eq: "Igual a",
                        neq: "Diferente a",
                        gte: "Mayor o igual a",
                        gt: "Mayor a",
                        lte: "Menor o igual a",
                        lt: "Menor a"
                    },
                    number: {
                        eq: "Igual a",
                        neq: "Diferente a",
                        gte: "Mayor o igual a",
                        gt: "Mayor a",
                        lte: "Menor o igual a",
                        lt: "Menor a"
                    },
                    string: {
                        startswith: "Inicia con",
                        endswith: "Termina con",
                        eq: "Es igual a",
                        neq: "Es diferente a",
                        contains: "Contiene",
                        doesnotcontain: "No Contiene"
                    }
                },
                messages: {
                    and: "Y",
                    or: "O",
                    filter: "Filtrar",
                    clear: "Limpiar",
                    info: "Mostrar los valores que sean"
                }
            },
            sortable: true,
            reorderable: true,
            resizable: true,
            editable: true,
            columnMenu: {
                messages: {
                    sortAscending: "Ordenar Ascendentemente",
                    sortDescending: "Ordenar Descendentemente",
                    columns: "Columnas",
                    filter: "Filtro Especial",
                }
            },
            pageable: {
                messages: {
                    display: "{0} - {1} de {2} Registros",
                    empty: "No existen datos",
                    page: "Página",
                    of: "de {0}",
                    itemsPerPage: "Páginas",
                    first: "Primero",
                    previous: "Anterior",
                    next: "Siguiente",
                    last: "Último",
                    refresh: "Refrescar"
                }
            }, dataBound: function (e) {
                $("#loading").fadeOut(200);
                setTimeout(
                    function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    }
                    , 100)
            }
        }

        // si es 0, buscamos el despacho que este en proceso
        if ($scope.id_despacho == 0) {
            DespachoService.despachos().lastDispatch(function (dispatch) {
                if (dispatch.id) {
                    $scope.id_despacho = dispatch.id
                    loadDispatch()
                }
                else {
                    $scope.notDispatchInProcess = true

                    setTimeout(function () {
                        window.location = "#/despachos/"
                    }, 1500);
                }
            })
        }
        else {
            loadDispatch()
        }

        // fin tabla de pedidos
    }
    ini()
    function loadDispatch() {
        DespachoService.despachos().query({ "id": $scope.id_despacho }, function (lista_pedidos) {

            $scope.fecha_despachado = lista_pedidos.fechaDespachado
            $scope.regs = lista_pedidos.detalle
            $("#gridPedidos .k-filter-row").hide();
            $scope.registrations.read();
        })
    }
    $scope.despachar = function () {
        var enviar_datos = {}
        enviar_datos.id = $scope.id_despacho
        enviar_datos.productos = []
        for (i = 0; i < $scope.registrations._data.length; i++) {
            enviar_datos.productos.push($scope.registrations._data[i])
        }
        DespachoService.despachos().update({ "id": $scope.id_despacho }, enviar_datos, function () {
            window.location.reload();
        })

    }
    $scope.exportar_despacho = function () {
        var rutaServicio = "/DespachoExport/" + $scope.id_despacho
        var params = {
            header: Base64.encode(localStorage.getItem('sJWT'))
        };
        window.open([connStringSVC.urlBase() + rutaServicio, $.param(params)].join('?'));
    }
    $scope.regresar = function () {
        window.location = "#/despachos/"
    }
}])