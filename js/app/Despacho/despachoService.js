HISProfile.factory('DespachoService', ['$resource', '$q', '$http', 'connStringSVC', '$cookieStore', function ($resource, $q, $http, connStringSVC, $cookieStore) {


    return {
        despachos: function () {
            return $resource(connStringSVC.urlBase() + '/Despacho/:id', { id: '@_id' }, {
                save: { method: 'POST', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                query: { method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                get: { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                update: { method: 'PUT', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                delete: { method: 'DELETE', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                agregarPedidosDespacho: { url: connStringSVC.urlBase() + '/DespachoAgregarPedidos/:id', method: 'PUT', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                lastDispatch: { url: connStringSVC.urlBase() + '/LastDispatch/', method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } }
            });
        },
    }

}])