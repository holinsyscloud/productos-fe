HISProfile.controller('despachoController', ['$scope', '$rootScope', 'DespachoService', 'PedidoService', function ($scope, $rootScope, DespachoService, PedidoService) {
    var pedidos_seleccionados = [];
    function ini() {
        // tabla de pedidos
        $scope.regsDespachos;
        $scope.mostrarFiltrosDespachos = false;
        $scope.registrationsDespachos = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    options.success($scope.regsDespachos);
                }
            },
            schema: {
                model: {
                    fields: {
                        fechaDespachado: { type: "date", editable: false },
                        created_date: { type: "date", editable: false },
                        usuario_creador: { type: "string", editable: false },
                        estado: { type: "string", editable: false },
                    }
                },

            },

            pageSize: 20,
            Paging: true,
        });
        $scope.registrationsColumnsDespachos = [

            {
                field: "created_date",
                title: "Fecha creación",
                format: "{0:dd/MM/yyyy HH:mm:ss}",
            },
            {
                field: "fechaDespachado",
                title: "Fecha Despacho",
                format: "{0:dd/MM/yyyy HH:mm:ss}",
            },
            {
                field: "estado",
                title: "Estado",
            },
            {
                field: "usuario_creador",
                title: "Usuario creador"
            },
            {
                command: [{ text: " ", template: '<img  src="img/iconos/x2/anadir-x2.png" class="icon_table" ng-if="dataItem.idEstado==1"  ng-click="crearEditarDespacho(\'editar\',dataItem)"/>' },
                { text: " ", template: '<img  src="img/buscarDocumento.png" class="icon_table" ng-click="verDespacho(dataItem)"/>' }]
            }

        ];
        $scope.gridOptionsDespachos = {
            excel: {
                fileName: "Despachos.xlsx",
                allPages: true,
                filterable: true,
            },
            height: "70vh",
            filterable: {
                mode: "row",
                operators: {
                    date: {
                        eq: "Igual a",
                        neq: "Diferente a",
                        gte: "Mayor o igual a",
                        gt: "Mayor a",
                        lte: "Menor o igual a",
                        lt: "Menor a"
                    },
                    number: {
                        eq: "Igual a",
                        neq: "Diferente a",
                        gte: "Mayor o igual a",
                        gt: "Mayor a",
                        lte: "Menor o igual a",
                        lt: "Menor a"
                    },
                    string: {
                        startswith: "Inicia con",
                        endswith: "Termina con",
                        eq: "Es igual a",
                        neq: "Es diferente a",
                        contains: "Contiene",
                        doesnotcontain: "No Contiene"
                    }
                },
                messages: {
                    and: "Y",
                    or: "O",
                    filter: "Filtrar",
                    clear: "Limpiar",
                    info: "Mostrar los valores que sean"
                }
            },
            sortable: true,
            reorderable: true,
            resizable: true,
            editable: true,
            columnMenu: {
                messages: {
                    sortAscending: "Ordenar Ascendentemente",
                    sortDescending: "Ordenar Descendentemente",
                    columns: "Columnas",
                    filter: "Filtro Especial",
                }
            },
            pageable: {
                messages: {
                    display: "{0} - {1} de {2} Registros",
                    empty: "No existen datos",
                    page: "Página",
                    of: "de {0}",
                    itemsPerPage: "Páginas",
                    first: "Primero",
                    previous: "Anterior",
                    next: "Siguiente",
                    last: "Último",
                    refresh: "Refrescar"
                }
            }, dataBound: function (e) {
                $("#loading").fadeOut(200);
                setTimeout(
                    function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    }
                    , 100)
            }
        }


        $scope.regsDespachos = DespachoService.despachos().get(function (lisat) {
            $("#gridDespachos .k-filter-row").hide();
            $scope.registrationsDespachos.read();
        })
        DespachoService.despachos().lastDispatch(function (dispatch) {
            if (dispatch.id) {
                $scope.dispatchProcessExist = true
            }
            else {
                $scope.dispatchProcessExist = false
            }
        })

        // fin tabla de pedidos
    }
    ini()

    function tablaListaPedidos() {
        $scope.regs;
        $scope.mostrarFiltros = false;
        $scope.registrations = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    options.success($scope.regs);
                }
            },
            schema: {
                model: {
                    fields: {
                        nombres: { type: "string", editable: false },
                        total: { type: "number", editable: false },
                        cantidadProductos: { type: "number", editable: false },
                        nombre: { type: "string", editable: false },
                        fecha: { type: "string", editable: false },
                        estado: { type: "string", editable: false },
                        idEstado: { type: "number", editable: false }
                    }
                },

            },

            pageSize: 20,
            Paging: true,
        });
        $scope.registrationsColumns = [
            {
                width: 100,
                title: "Seleccionar",
                command: [
                    {
                        text: "Pagar",
                        template: '<label class="control control--checkbox" style=" display: block; font-weight: normal; text-align: center"><input type="checkbox" ng-click="seleccionar_pedido(dataItem)"/><div class="control__indicator"></div></label>'
                    }
                ]
            },

            {
                field: "nombres",
                title: "Nombres"
            },
            {
                field: "total",
                title: "Total (S/.)"
            },
            {
                field: "cantidadProductos",
                title: "Productos"
            },

            {
                field: "fecha",
                title: "Fecha",
            },

        ];
        $scope.gridOptions = {
            excel: {
                fileName: "Cotizaciones.xlsx",
                allPages: true,
                filterable: true,
            },
            height: "70vh",
            filterable: {
                mode: "row",
                operators: {
                    date: {
                        eq: "Igual a",
                        neq: "Diferente a",
                        gte: "Mayor o igual a",
                        gt: "Mayor a",
                        lte: "Menor o igual a",
                        lt: "Menor a"
                    },
                    number: {
                        eq: "Igual a",
                        neq: "Diferente a",
                        gte: "Mayor o igual a",
                        gt: "Mayor a",
                        lte: "Menor o igual a",
                        lt: "Menor a"
                    },
                    string: {
                        startswith: "Inicia con",
                        endswith: "Termina con",
                        eq: "Es igual a",
                        neq: "Es diferente a",
                        contains: "Contiene",
                        doesnotcontain: "No Contiene"
                    }
                },
                messages: {
                    and: "Y",
                    or: "O",
                    filter: "Filtrar",
                    clear: "Limpiar",
                    info: "Mostrar los valores que sean"
                }
            },
            sortable: true,
            reorderable: true,
            resizable: true,
            editable: true,
            columnMenu: {
                messages: {
                    sortAscending: "Ordenar Ascendentemente",
                    sortDescending: "Ordenar Descendentemente",
                    columns: "Columnas",
                    filter: "Filtro Especial",
                }
            },
            pageable: {
                messages: {
                    display: "{0} - {1} de {2} Registros",
                    empty: "No existen datos",
                    page: "Página",
                    of: "de {0}",
                    itemsPerPage: "Páginas",
                    first: "Primero",
                    previous: "Anterior",
                    next: "Siguiente",
                    last: "Último",
                    refresh: "Refrescar"
                }
            }, dataBound: function (e) {
                $("#loading").fadeOut(200);
                setTimeout(
                    function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    }
                    , 100)
            }
        }

    }
    $scope.crearEditarDespacho = function (accion, datosEdicion) {
        if (accion == 'crear') {
            $scope.accionCrear = true
            $scope.accionEditar = false
        }
        else {
            $scope.accionEditar = true
            $scope.accionCrear = false
            $scope.idDespachoEditar = datosEdicion.id
        }
        $scope.mostrar_creacion = true
        tablaListaPedidos()
        $scope.regs = PedidoService.Pedidos().query({ "idEstado": 3, "idTipo": 1 }, function (lista_pedidos) {
            $("#gridPedidos .k-filter-row").hide();
            $scope.registrations.read();
        })
    }
    $scope.seleccionar_pedido = function (dataItem) {
        var id_pedido_existente
        var existe_pedido = false
        $scope.mostrar_error = false
        for (i = 0; i < pedidos_seleccionados.length; i++) {

            if (pedidos_seleccionados[i] == dataItem.id) {
                id_pedido_existente = i
                existe_pedido = true
                break;
            }
        }
        if (existe_pedido) {
            pedidos_seleccionados.splice(id_pedido_existente, 1);
        }
        else {
            pedidos_seleccionados.push(dataItem.id)
        }
    }
    $scope.crear_despacho = function () {
        $scope.mostrar_error = false
        if (pedidos_seleccionados.length > 0) {
            DespachoService.despachos().save({ "pedidos": pedidos_seleccionados }, function (despacho_creado) {
                if (despacho_creado.id) {
                    window.location = "#/despachos/" + despacho_creado.id;
                }
            })
        }
        else {
            $scope.mostrar_error = true
        }

    }
    $scope.agregarPedidosDespacho = function () {
        $scope.mostrar_error = false
        if (pedidos_seleccionados.length > 0) {
            DespachoService.despachos().agregarPedidosDespacho({ id: $scope.idDespachoEditar }, { "pedidos": pedidos_seleccionados }, function (despachoActualizado) {
                if (despachoActualizado.id) {
                    window.location = "#/despachos/" + despachoActualizado.id;
                }

            })
        }
        else {
            $scope.mostrar_error = true
        }

    }
    $scope.regresar = function () {
        $scope.mostrar_creacion = false
    }
    $scope.verDespacho = function (dataItem) {
        window.location = "#/despachos/" + dataItem.id;
    }
}])