HISProfile.controller('confdespController', ['$scope', '$rootScope', 'PedidoService', function ($scope, $rootScope, PedidoService) {
    
    function ini(){
        PedidoService.PedidosConfDesp().query(function (lista_pedidos) {
            $scope.pedidosPorConf = lista_pedidos;
        })
    }
    ini()
    
    $scope.confirmar = function(id, nombre){
        $scope.idPedidoMostrando = id
        $scope.nombrePedido = nombre
        
        $("#popupConfirmar").modal('toggle')
    }
    $scope.aceptar_pedido = function(id){
         PedidoService.PedidosConfDesp().responder_pedido({ "id": id }, { "modo": 'Y'}, function (estado_pedidos) {
            ini();
              $("#popupConfirmar").modal('toggle')
        })
    }
    $scope.rechazar_pedido = function(id){
            PedidoService.PedidosConfDesp().responder_pedido({ "id": id }, { "modo": 'N'}, function (estado_pedidos) {
            ini();
              $("#popupConfirmar").modal('toggle')
        })   
    }
}])