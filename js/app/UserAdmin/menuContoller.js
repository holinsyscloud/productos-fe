angular.module('holinsysMenuController', [])


  .controller('menuController', ['$cookieStore', '$scope', 'Session', function ($cookieStore, $scope, Session) {



    $scope.token = $cookieStore.get('tokenActual');


    Session.datosFuncionalidades($scope.token).then(function (responseMessage) {
      //    $scope.hideLoading();




      $scope.modulos = [];

      for (i = 0; i < responseMessage.data.length; i++) {
        var agregado = false;
        for (j = 0; j < $scope.modulos.length; j++) {
          if (responseMessage.data[i].modulo === $scope.modulos[j].nombre) {
            $scope.modulos[j].funcionalidades.push(responseMessage.data[i]);
            agregado = true;
          }
          if (agregado)
            break;
        }
        if (agregado == false) {
          var itemModulo = {};
          itemModulo.nombre = responseMessage.data[i].modulo;
          itemModulo.funcionalidades = [];
          itemModulo.funcionalidades.push(responseMessage.data[i]);
          $scope.modulos.push(itemModulo);
        }

      }




      if (responseMessage.data.Code == "000000") {
        localStorage["Token"] = responseMessage.token;
        window.location.reload();
      }
    }, function (error) {



      var status = error.status;



      if (status == 401) {
        $scope.errorTittle = "Sesion Cerrada";
        $scope.errorMensaje = "Porfavor vuelva a inicar Sesion";
        $scope.errorFuncion = "cierreDeSesion";
        $('#popupErrores').modal('show');


      };


    });

    $scope.cierreDeSesion = function () {

      $cookieStore.remove('tokenActual');
      location.reload();
      window.location = "/#/";
    }
    $scope.cerrarSesion = function () {

      Session.cerrarSesion($scope.token).then(function (responseMessage) {

        $cookieStore.remove('tokenActual');
        $scope.token = $cookieStore.get('tokenActual');
        window.location = "/#/";
      });

    };




  }])