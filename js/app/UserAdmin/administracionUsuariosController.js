HISUserAdmin.controller('administracionUsuariosController', ['$compile','$cookieStore','$scope','Session','erroresServices','SuscripcionesService', function($compile,$cookieStore,$scope,Session,erroresServices,SuscripcionesService){

  $scope.ini=function()
  {
      $scope.formData = {
        "roles":[],
          "confirmacionCreacion": 0
      };

      var cantidadPaginacion= 10;
      var inicioPaginacion= 0;
      $scope.mostrarFiltros=false;
        $scope.regs;

       $scope.registrations = new kendo.data.DataSource({
          transport: {
              read: function(options) {
                   options.success($scope.regs);
              }
          },
        
           schema: {
                   model: {

                            fields: {
                                id: { type: "string" }, 
                                nombres: { type: "string" },                                
                                correo: { type: "string" },                                
                                estadoTexto: { type: "string" }
                             
                            }
                      }
                    },
          pageSize: 20,
          Paging: true,
      });


      $scope.registrationsColumns = [       
                              {field:"nombres",
                                title:"Nombre",
                              },                              

                              {field:"correo",
                                title:"Correo",
                              },

                              {field:"estadoTexto",
                                title:"Estado",                                
                              },

       {command:  [{ text: " ", template: '<kendo-button sprite-css-class="\'k-icon k-i-pencil\'" ng-click="mostrarEdicionUsuarios(dataItem)"></kendo-button>'}
                                , { text: " ", template: '<kendo-button sprite-css-class="\'k-icon k-i-close\'"  ng-click="eliminacionDeUsuarios(dataItem)" ></kendo-button>'}]}
                             
                ];



            $scope.gridOptions = {                 
            
              
            filterable: {
                                  mode: "row",
                                  operators: {
                                      date: {
                                          eq: "Igual a",
                                          neq: "Diferente a",
                                          gte: "Mayor o igual a",
                                          gt: "Mayor a",  
                                          lte: "Menor o igual a",
                                          lt: "Menor a"
                                      },
                                      number: {
                                          eq: "Igual a",
                                          neq: "Diferente a",
                                          gte: "Mayor o igual a",
                                          gt: "Mayor a",  
                                          lte: "Menor o igual a",
                                          lt: "Menor a"                                    
                                      },
                                      string: {
                                          startswith: "Inicia con",
                                          endswith: "Termina con",
                                          eq: "Es igual a",
                                          neq: "Es diferente a",
                                          contains:"Contiene",
                                          doesnotcontain:"No Contiene"
                                      }
                                     
                                  },
                                  messages:{
                                    and:"Y",
                                    or:"O",
                                    filter:"Filtrar",
                                    clear:"Limpiar",
                                    info:"Mostrar los valores que sean"
                                  }
                          },    
                sortable: true,
                reorderable: true,
                resizable: true,   
                 editable: false, 
                 columnMenu: {
                    messages:{
                      sortAscending:"Ordenar Ascendentemente",
                      sortDescending:"Ordenar Descendentemente",
                      columns:"Columnas",
                      filter:"Filtro Especial",
                    }
                 } ,        
                pageable: {
                    messages: {
                        display: "{0} - {1} de {2} Registros", 
                        empty: "No existen datos",
                        page: "Página",
                        of: "de {0}",
                        itemsPerPage: "Páginas",
                        first: "Primero",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Último",
                        refresh: "Refrescar"
                    }
                },
                height:380,
                
            };

        $scope.regs = Session.UAUsers().query(function(result) {
              $scope.registrations.read();
              $("#gridUAClienteLista .k-filter-row").hide()
              
        });

      $scope.reanudarPantalla = function () {
            location.reload();
          
      }


      $scope.cierreDeSesion = function () {

        $cookieStore.remove('tokenActual');
         location.reload();
         window.location= "/#/";
      }
     
      $scope.submitForm = function (isValid,TipodeGrabacion) {

          // agregar usuario           
        $scope.submitted=true
        if(isValid)  
        {
          if(TipodeGrabacion==0 || TipodeGrabacion==1){


                if(TipodeGrabacion==1){
                  $scope.formData.confirmacionCreacion = 1;        
                };
                 $scope.formData.rol=$scope.rolesUsuariosOption.rol;
                 $scope.formData.suscripcion=$scope.suscripcionElegida;
         
                $scope.datos = JSON.stringify($scope.formData);
                
                
                Session.UAUsers().save($scope.datos, function(savedRecord) {
                        $scope.submitted=false;
                        $('#popupNuevoUsuario').modal('hide');
                  
                     $scope.regs = Session.UAUsers().query(function(result) {
                      $scope.registrations.read();
                   });
                      $scope.formData = {
                      "roles":[],
                        "confirmacionCreacion": 0
                    };

                },function(error){
                  

                });               

            }
            if(TipodeGrabacion==2){


                        Session.ReactivarUsuario($cookieStore.get('tokenActual'),$scope.idUsuario).then(function(responseMessage){
                        $('#popupNuevoUsuario').modal('hide');
                        $('#popupUsuarioExistente').modal('hide');
                        $scope.errorTittle ="Reactivación de usuario";
                        $scope.errorMensaje ="Se reactivo el usuario";
                        $scope.errorFuncion= "reanudarPantalla";
                        $('#popupErrores').modal('show'); 
                        
                        },function(error){
                   
                          var status = error.status;
                          if(status==400){
                          //usuario existe no puede ser reemplazado
                              $scope.errorTittle ="Usuario Existente";
                              $scope.errorMensaje ="El usuario no esta dado de baja no puede reactivarse";
                              $scope.errorFuncion= "reanudarPantalla";
                              $('#popupErrores').modal('show');    
                          
                          };

                          if(status==401){           
                         //Error de token
                               $scope.errorTittle ="Sesion Cerrada";
                               $scope.errorMensaje ="Porfavor vuelva a inicar Sesion";
                               $scope.errorFuncion= "cierreDeSesion";
                               $('#popupErrores').modal('show')    ;
                          };

                          if(status==500){           
                     
                                $scope.errorTittle ="Error Sistema";
                                $scope.errorMensaje ="Error conectado con el servidor";
                                $scope.errorFuncion= "ningunaFuncion";
                                $('#popupErrores').modal('show');
                          };
                        });
                  }
     

        }

      };
  }

  $scope.ini()

      $scope.EliminarUsuario = function () {

          
          Session.UAUsers().delete({id: $scope.eliminarIdUsuario},function(responseMessage) {                 

                    $('#popupEliminarUsuario').modal('hide');
   
             $scope.regs = Session.UAUsers().query(function(result) {
                  $scope.registrations.read();
            });      

          },function(error){



               var status = error.status;
               if(status==401){
                     
                
                };

                if(status==500){
                   
                  
                };


            }); 

      };


      $scope.ListarRoles = function (llegada) {
          $scope.rolesUsuariosOption={}
          $scope.ListaRoles= Session.UserRoles().get(function(result) {                   
              // $scope.rolesUsuariosOption.rol=result[0]._id
              $scope.Suscripciones()
          });
      };

      $scope.Suscripciones=function()
      {
          $scope.suscripciones=SuscripcionesService.suscripciones().get(function(result) {             
              $scope.suscripcionElegida=result[0].codigo_tenant
              // $scope.rolesUsuariosOption.rol=result[0]._id
          });
      }
    $scope.showFiltros=function()
  {
      $("#gridUAClienteLista .k-filter-row").show() //muestra filtros Grid
    $scope.mostrarFiltros=true;
  }
  $scope.hideFiltros=function()
  {
    $("#gridUAClienteLista .k-filter-row").hide()  //oculta filtros Grid
    $scope.mostrarFiltros=false;
  }

     
      $scope.eliminacionDeUsuarios = function(dataItem){
          $scope.eliminarIdUsuario=dataItem._id;
          $('#popupEliminarUsuario').modal('show');

         
      }

      $scope.mostrarEdicionUsuarios = function(dataItem){
          /* Se prepara un objeto adicional para evitar 
             cambiar la informacion del registro de la
             grilla sin haber hecho click en el boton
             'Actualizar'.
          */
          
       
          $scope.ListarRoles('editar');
          $scope.tempDataItem = dataItem;
          

          if(dataItem.estado=="H")
          {
            $scope.valorHabilitado=true;
          }
          else
          {
            $scope.valorHabilitado=false;
          }
          $scope.editData ={
                id: dataItem._id,
                // codigo: dataItem.usuario,
                nombres: dataItem.nombres,
                // apellidos: dataItem.apellidos,
                correo: dataItem.correo,
                // telefonoContacto: dataItem.telefono,
                // DNI: dataItem.dni,
                rol:dataItem.rol,

          };

          $scope.ListaRoles= Session.UserRoles().get(function(result) {   
               
            });

          var data=dataItem.rol;
          var dataarray=data.split(",");
          $scope.rolesUsuariosOption={
            rol: []
          }
          
          $scope.rolesUsuariosOption.rol=dataarray
          
             
          $('#popupEditarUsuario').modal('show');
      
      };

      $scope.onChange = function(cbState) {
    $scope.message = cbState;
  };

       $scope.editarUsuario= function(data,isValid){

          $scope.submitted=true
          if(isValid)
          {
              // data.roles=[];
              $scope.editData.rol=$scope.rolesUsuariosOption.rol
              $scope.editData.suscripcion=$scope.suscripcionElegida;
              if($scope.valorHabilitado)
              {
                $scope.editData.estado="H"  
              }
              else
              {
                $scope.editData.estado="B" 
              }             
              
              Session.UAUsers().updateUser({id:$scope.editData.id},$scope.editData, function() {
                 $scope.tempDataItem.nombres = $scope.editData.nombres;
                 $scope.regs = Session.UAUsers().query(function(result) {
                          $scope.registrations.read();
                           $("#gridUAClienteLista .k-filter-row").hide()  
                 });
                 $scope.submitted=false
                 $('#popupEditarUsuario').modal('hide'); 

              },function (error){

                     erroresServices.controlError(error.status);

              });
          }
             
            
      };



}])