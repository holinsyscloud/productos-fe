HISUserAdmin.factory('Session',['$q','$http','connStringSVC','$cookieStore','$resource',function($q,$http,connStringSVC,$cookieStore,$resource){
 return {
   UAUsers:function(){
        return $resource(connStringSVC.urlBase() + '/HS_Usuario/:id', { id: '@_id' }, {
          save:    { method: 'POST', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  } },
          query:   { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  } },
          get:     { method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  }},
          update:  { method: 'PUT' , headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  }},
          updateUser:  { url: connStringSVC.urlBase() + '/HS_Usuario/actualizarUsuario/:id',method: 'PUT' , headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  }},          
          delete:  { method: 'DELETE' , headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  }}
        });

      },

     UserRoles:function(){
        return $resource(connStringSVC.urlBase() + '/HS_RolFuncion/:id', { id: '@_id' }, {
          save:    { method: 'POST', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  } },
          query:   { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  } },
          get:     { method: 'GET', isArray: true,headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  }},
          update:  { method: 'PUT' , headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  }},
          delete:  { method: 'DELETE' , headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  }}
        });

      },
      apiKey:function(){
        return $resource(connStringSVC.urlBase() + '/HS_apiKey/:id', { id: '@_id' }, {
          save:    { method: 'POST', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  } },
          query:   { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  } },
          get:     { method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  }},
          update:  { method: 'PUT' , headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  }},
          delete:  { method: 'DELETE' , headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  }}
        });

      },


  grabarUsuario: function(token,datos){
      var deferred=$q.defer();

       //$http.post(connectionString.urlBase() +'Autenticar/LoginUsuario' + token)
       $http({
                method: 'POST',
                skipAuthorization: true,
                url: connStringSVC.urlBase() +'Usuario/RegistrarUsuario?token=' + token,
                headers: {'Content-Type': 'application/json'},
                data: datos
            })
       .then(function(res)
            {
                deferred.resolve(res);
            }, function(error)
            {
                deferred.reject(error);
            });
       return deferred.promise;
     },

  editarUsuario: function(token,datos){
    var deferred=$q.defer();
    $http({
              method: 'PUT',
              skipAuthorization: true,
              url: connStringSVC.urlBase() +'Usuario/ActualizarUsuario?token=' + token,
              headers: {'Content-Type': 'application/json'},
              data: datos
          })
   .then(function(res)
            {
                deferred.resolve(res);
            }, function(error)
            {
                deferred.reject(error);
            });
    return deferred.promise;
  },



  ReactivarUsuario: function(token,idUsuario){
      var deferred=$q.defer();
     
       //$http.post(connectionString.urlBase() +'Autenticar/LoginUsuario' + token)
       $http({
                method: 'PUT',
                skipAuthorization: true,
                url: connStringSVC.urlBase() +'Usuario/ReactivarUsuario?token=' + token + "&idUsuario=" + idUsuario,
                headers: {'Content-Type': 'application/json'}
                
            })
       .then(function(res)
            {
                deferred.resolve(res);
            }, function(error)
            {
                deferred.reject(error);
            });
       return deferred.promise;
     },


     EliminarUsuario: function(token,idUsuario){
      var deferred=$q.defer();
     
       //$http.post(connectionString.urlBase() +'Autenticar/LoginUsuario' + token)
       $http({
                method: 'DELETE',
                skipAuthorization: true,
                url: connStringSVC.urlBase() +'Usuario/EliminarUsuario?token=' + token + "&idUsuario=" + idUsuario,
                headers: {'Content-Type': 'application/json'}
                
            })
       .then(function(res)
            {
                deferred.resolve(res);
            }, function(error)
            {
                deferred.reject(error);
            });
       return deferred.promise;
     },

      UARoles:function(){
        return $resource(connStringSVC.urlBase() + '/HS_Roles/:id', { id: '@_id' }, {
          save:    { method: 'POST', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  } },
          query:   { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  } },
          get:     { method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  }},
          update:  { method: 'PUT' , headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  }},
          delete:  { method: 'DELETE' , headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  }}
        });
        
      }

  

        
};
}])


