HISShared.controller('menuController',['$scope','$filter','$rootScope','menuService','Session','ProfileService','$cookieStore', function($scope,$filter,$rootScope,menuService,Session,ProfileService,$cookieStore){

  menuService.listarMenu().query(function(result) {
    $scope.modulos = [];
    for (i = 0; i < result.length; i++){
      var agregado = false;
      for (j=0; j< $scope.modulos.length; j++){
        if (result[i].modulo === $scope.modulos[j].nombre){
          $scope.modulos[j].funcionalidades.push(result[i]);
          agregado = true;
        }

        if (agregado)
          break;
      }

      if (agregado == false){
        var itemModulo = {};
        itemModulo.nombre = result[i].modulo;
        itemModulo.funcionalidades = [];
        itemModulo.funcionalidades.push(result[i]);
        $scope.modulos.push(itemModulo);
      }
    }
  });

  $scope.notificationsArray = [];



  $scope.llamarFuncion = function(item){
    console.log(item);
    if ((item) && (item.funcion)){

      switch(item.funcion) {

        case "0":{
          window.location= "#/cxc/facturas/detalle/56bd0dabdd89f2e5edb2c8a3";
          break;
        }

        case "1":{
          $("#tabStrip").data("kendoTabStrip").select(1);
          break;
        }

        //facturas Recursivas
        case "2":{
          //$scope.popUpFacturasRecurrentes()
          console.log("antes de broadcast");
          $scope.$broadcast ('eventFactRecurrentes');
          //$('#popupFacturasRecursivas').modal('show');
         // $scope.popUpFacturasRecurrentes()
          break;
        }

      }

    }

  };

  $scope.miApiKey=function(){
    Session.apiKey().get(function(savedRecord) {      

      if(savedRecord.apiKey)
      {
        $scope.idApiKey=savedRecord.idApikey
        $scope.apiKeyExiste=true
        $scope.apiKeyGenerada=savedRecord.apiKey                
      }
      else
      {
        $scope.idApiKey=" "
        $scope.apiKeyExiste=false
      }  
      $('#popupApiKey').modal('show');  
    });  
    
  }

  $scope.generarApiKey=function()
  {
    Session.apiKey().save({},function(savedRecord) {    
    
      $scope.apiKeyGenerada=savedRecord.apiKey
      $scope.idApiKey=savedRecord.idApiKey
      $scope.apiKeyExiste=true
    });  
  }
  $scope.reGenerarApiKey=function()
  {
    Session.apiKey().update({id:$scope.idApiKey},{}, function(savedRecord) {
        $scope.apiKeyGenerada=savedRecord.apiKey
        $scope.idApiKey=savedRecord.idApiKey
    }); 
  }

  $scope.cerrarSesion =function(){

      $cookieStore.remove('tokenActual');
      ProfileService.Perfil().delete({}, function() {
        localStorage.clear();
        window.location= "/#/";
      },function (error){
        localStorage.clear();
        window.location= "/#/";        
      });

  };

}]),

HISShared.controller('directivaMostrarAgregarDatosController',['$scope','$filter','$rootScope','directivaMostrarAgregarDatosServices',function($scope,$filter,$rootScope,directivaMostrarAgregarDatosServices){
          
          $scope.existe=true;

          $scope.datosContactos={}
          $scope.cargarDatos=function(){
            
            $scope.datosContactos= directivaMostrarAgregarDatosServices.directivaAgregar($scope.modelo.modelo).query($scope.modelo.filtro,function(result) {  
                
            });
          }

          $scope.validarExistencia=function() {
              $scope.existe=false;
         
              for(i=0; i<$scope.datosContactos.length;i++)
              {    
                   
                    if($scope.datosMostrarContacto[$scope.modelo.placeholder] == $scope.datosContactos[i].nombre){
                        $scope.existe=true;
                        break;
                    };
              };
              
                            
          }

          $scope.crearNuevoValor=function(){
              $scope.datosGuardar={
                nombre:$scope.datosMostrarContacto[$scope.modelo.placeholder],
                tipo:$scope.modelo.tipo
              };
              directivaMostrarAgregarDatosServices.directivaAgregar($scope.modelo.modelo).save($scope.datosGuardar, function() {
                    $scope.existe=true;
                    $scope.cargarDatos();
              }); 
          }
          $scope.cargarDatos();
     
}])