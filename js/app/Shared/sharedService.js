HISShared.factory('connStringSVC', function () {
  
  var serviciosBase="http://itpback.holinsys.net";

//  var serviciosBase="http://olimpo.holinsys.net:8071"; 
  //var serviciosBase="http://qairaback.holinsys.net:8071";
  //var serviciosBase = "http://localhost:8170";

  return {
    urlBase: function () {
      return serviciosBase;
    },
    getToken: function () {
      return localStorage.getItem('sJWT');
    }
  }
})

HISShared.factory('mapKey', function () {
  var mapKey = "AnQTcanGcUvsE5KDCE2WrgXES5_Yxn-7nLSMDEYoIiK3a3jLYQIjmdQ4yjWQr9ep";
  return {

    getKey: function () {
      return mapKey;
    }
  }

});

HISShared.factory('loadingCounts', function () {
  return {
    enable_count: 0,
    disable_count: 0
  }
});


HISShared.factory('sharedValidationsSVC', function () {
  return {

    validarRangoFecha: function (fechaInicio, fechaFin) {

      /* Extracción de los dias meses y años de cada fecha*/

      var valDiaInicio = fechaInicio.substring(0, 2);
      var valMesInicio = fechaInicio.substring(3, 5);
      var valAnioInicio = fechaInicio.substring(6, 10);

      var valDiaFin = fechaFin.substring(0, 2);
      var valMesFin = fechaFin.substring(3, 5);
      var valAnioFin = fechaFin.substring(6, 10);

      /* Fin de Extracción de los dias meses y años de cada fecha*/


      var validacionFecha = false

      if (valDiaInicio <= valDiaFin && valMesInicio <= valMesFin && valAnioInicio <= valAnioFin) {
        validacionFecha = true
      }

      return validacionFecha;
    },




    validarDinero: function (dinero) {

      var validacionDinero = true


      if (dinero <= 0) {
        validacionDinero = false
      }

      return validacionDinero;
    }


  }
}),

  HISShared.factory('sharedFormateoUsaSVC', function () {
    return {

      fechaUsa: function (fecha) {

        var valDia = fecha.substring(0, 2);
        var valMes = fecha.substring(3, 5);
        var valAnio = fecha.substring(6, 10);
        var fechaFormatoUsa = valMes + '/' + valDia + '/' + valAnio;

        return fechaFormatoUsa

      }

    }
  }),

  HISShared.factory('extraerDocumentosParametro', function () {
    return {


      extraerDocumentos: function (cadena) {

        var indexDocumentos = [];
        for (var pos = cadena.indexOf(';'); pos !== -1; pos = cadena.indexOf(';', pos + 1)) {
          indexDocumentos.push(pos);
        }

        var documentos = []
        var valorInicial = 0
        var valorFinal = indexDocumentos[0]
        for (i = 0; i <= indexDocumentos.length; i++) {
          documentos.push({ documento: cadena.substring(valorInicial, valorFinal) })
          valorInicial = indexDocumentos[i] + 1
          if (i != indexDocumentos.length - 1) {
            valorFinal = indexDocumentos[i + 1]
          }
          else {
            valorFinal = cadena.length
          }
        }

        var documentosCxC = []
        for (i = 0; i < documentos.length; i++) {
          var indexDocumentos = [];
          for (var pos = documentos[i].documento.indexOf('-'); pos !== -1; pos = documentos[i].documento.indexOf('-', pos + 1)) {
            indexDocumentos.push(pos);
          }
          documentosCxC.push({ documento: documentos[i].documento.substring(0, indexDocumentos[0]), url: documentos[i].documento.substring(indexDocumentos[0] + 1) })
        }

        return documentosCxC
      }


    }
  }),
  HISShared.factory('fileAppServices', ['$resource', 'connStringSVC', function ($resource, connStringSVC) {

    return {


      obtenerArchivos: function (modelo) {
        return $resource(connStringSVC.urlBase() + '/upload/:id', { id: '@_id' }, {

          query: { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
          delete: { method: 'DELETE', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } }

        });

      },


    }
  }]),


  HISShared.factory('menuService', ['$resource', 'connStringSVC', function ($resource, connStringSVC) {

    return {


      listarMenu: function () {
        return $resource(connStringSVC.urlBase() + '/HS_Menu/:id', { id: '@_id' }, {
          query: { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
        });



      },


    }
  }]),
  HISShared.factory('conceptoServices', ['$resource', 'connStringSVC', function ($resource, connStringSVC) {

    return {


      concepto: function () {
        return $resource(connStringSVC.urlBase() + '/CMConceptos/:id', { id: '@_id' }, {

          save: { method: 'POST', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
          query: { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
          filter: { url: connStringSVC.urlBase() + '/CMConceptosFilter/', method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },

          get: { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
          update: { method: 'PUT', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
          delete: { method: 'DELETE', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } }
        });

      },


    }
  }]),
  HISShared.factory('erroresServices', ['$resource', 'connStringSVC', function ($resource, connStringSVC) {

    return {


      controlError: function (status) {
        /* if(status==500){
                      // Pop up de error de servicio generico
          }else if(status==401){
                  window.location= "login.html";
          };*/

      },


    }
  }]),
  HISShared.factory('contactosServices', ['$resource', 'connStringSVC', function ($resource, connStringSVC) {

    return {


      contactos: function () {
        return $resource(connStringSVC.urlBase() + '/HS_Contactos/:id', { id: '@_id' }, {

          save: { method: 'POST', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
          query: { method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
          get: { method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
          update: { method: 'PUT', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
          delete: { method: 'DELETE', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } }
        });

      },


    }
  }]),
  HISShared.factory('bancosServices', ['$resource', 'connStringSVC', function ($resource, connStringSVC) {

    return {


      bancos: function () {
        return $resource(connStringSVC.urlBase() + '/CMBancos/:id', { id: '@_id' }, {

          save: { method: 'POST', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
          query: { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
          get: { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
          update: { method: 'PUT', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
          delete: { method: 'DELETE', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } }
        });

      },


    }
  }]),
  HISShared.factory('directivaMostrarAgregarDatosServices', ['$resource', 'connStringSVC', function ($resource, connStringSVC) {

    return {


      directivaAgregar: function (modelo) {
        return $resource(connStringSVC.urlBase() + '/' + modelo + '/:id', { id: '@_id' }, {

          save: { method: 'POST', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
          query: { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
          get: { method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
          update: { method: 'PUT', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
          delete: { method: 'DELETE', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } }
        });

      },


    }
  }]),
  HISShared.factory('eliminadoService', ['$resource', 'connStringSVC', function ($resource, connStringSVC) {

    return {

      eliminados: function () {
        return $resource(connStringSVC.urlBase() + '/Eliminado/:id', { id: '@_id' }, {
          delete: { method: 'DELETE', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } }
        });

      },


    }
  }]),
  HISShared.factory('listaFacturasService', ['$resource', 'connStringSVC', function ($resource, connStringSVC) {

    return {
      facturasListado: function () {
        return $resource(connStringSVC.urlBase() + '/listaFacturas/:id', { id: '@_id' }, {
          query: { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
          get: { method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
        });
      }
    }

  }]),
  HISShared.factory('mensajesData', ['$resource', 'connStringSVC', function ($resource, connStringSVC) {

    return {

      mensajeSave: function (cb) {

        $('#popupMensajesSave').modal('show');
        setTimeout(function () {
          $('#popupMensajesSave').modal('hide');
          setTimeout(function () {
            return cb(200);
          }, 800);
        }, 2000);



      },
      mensajeDelete: function (cb) {
        $('#popupMensajesDelete').modal('show');
        setTimeout(function () {
          $('#popupMensajesDelete').modal('hide');
          setTimeout(function () {
            return cb(200);
          }, 800);
        }, 2000);
      },
      mensajeUpdate: function (cb) {

        $('#popupMensajesUpdate').modal('show');
        setTimeout(function () {
          $('#popupMensajesUpdate').modal('hide');
          setTimeout(function () {
            return cb(200);
          }, 800);
        }, 2000);
      },
      mensajeErrorCheque: function (cb) {

        $('#popupMensajesErrorCheque').modal('show');
        setTimeout(function () {
          $('#popupMensajesErrorCheque').modal('hide');
          setTimeout(function () {
            return cb(200);
          }, 800);
        }, 2000);
      },


    }
  }])

