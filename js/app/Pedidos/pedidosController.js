HISProfile.controller('PedidosController', ['$scope', '$rootScope', 'PedidoService', function ($scope, $rootScope, PedidoService) {
    function ini() {
        // tabla de pedidos

        $scope.regs;
        $scope.mostrarFiltros = false;
        $scope.registrations = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    options.success($scope.regs);
                }
            },
            schema: {
                model: {
                    fields: {
                        nombres: { type: "string", editable: false },
                        total: { type: "number", editable: false },
                        cantidadProductos: { type: "number", editable: false },
                        comentario: { type: "string", editable: false },
                        razonRechazo: { type: "string", editable: false },
                        modoPago: { type: "string", editable: false },
                        nombre: { type: "string", editable: false },
                        fecha: { type: "string", editable: false },
                        estado: { type: "string", editable: false },
                        idEstado: { type: "number", editable: false },
                        vendedor: { type: "string", editable: false }
                    }
                },

            },

            pageSize: 20,
            Paging: true,
        });
        $scope.registrationsColumns = [

            {
                field: "nombres",
                title: "Nombres"
            },
            {
                field: "vendedor",
                title: "Vendedor"                
            },
            {
                field: "total",
                title: "Total (S/.)",
                width: 150,
                template: '<div style="text-align:center;">{{dataItem.total}}</div>'
            },
            {
                field: "modoPago",
                title: "Modo de Pago",
                width: 150
            },
            {
                field: "estado",
                title: "Estado",
                width: 170
            },
            {
                field: "fecha",
                title: "Fecha",
                type: "date",
                format: "{0:dd/MM/yyyy}",
                width: 150
            },
            {
                title: "Notas",
                width: 100,
                attributes: { style: "text-align:center;" },
                command: [{ text: " ", template: '<kendo-button sprite-css-class="\'k-icon k-i-pencil\'" ng-click="showOrder(dataItem)"></kendo-button>' }]
            },

            {
                title: "Aprobar",
                attributes: { style: "text-align:center;" },
                width: 100,
                command: [{ text: " ", template: '<img src="/img/check.png" ng-if="dataItem.idEstado==2" ng-click="aceptar_pedido(dataItem.id)"/>' }
                    , { text: " ",
                    template: '<img src="/img/error.png" ng-if="dataItem.idEstado!=4 && dataItem.idEstado!=5 && dataItem.idEstado!=6 && dataItem.idEstado!=11 && dataItem.idEstado!=12   && dataItem.idEstado!=10" ng-click="mostrarRechazarPedido(dataItem)" style="margin-left: 5px;"/>' }]
            }
        ];
        $scope.gridOptions = {
            excel: {
                fileName: "Cotizaciones.xlsx",
                allPages: true,
                filterable: true,
            },
            height: "70vh",
            filterable: {
                mode: "row",
                operators: {
                    date: {
                        eq: "Igual a",
                        neq: "Diferente a",
                        gte: "Mayor o igual a",
                        gt: "Mayor a",
                        lte: "Menor o igual a",
                        lt: "Menor a"
                    },
                    number: {
                        eq: "Igual a",
                        neq: "Diferente a",
                        gte: "Mayor o igual a",
                        gt: "Mayor a",
                        lte: "Menor o igual a",
                        lt: "Menor a"
                    },
                    string: {
                        startswith: "Inicia con",
                        endswith: "Termina con",
                        eq: "Es igual a",
                        neq: "Es diferente a",
                        contains: "Contiene",
                        doesnotcontain: "No Contiene"
                    }
                },
                messages: {
                    and: "Y",
                    or: "O",
                    filter: "Filtrar",
                    clear: "Limpiar",
                    info: "Mostrar los valores que sean"
                }
            },
            sortable: true,
            reorderable: true,
            resizable: true,
            editable: true,
            columnMenu: {
                messages: {
                    sortAscending: "Ordenar Ascendentemente",
                    sortDescending: "Ordenar Descendentemente",
                    columns: "Columnas",
                    filter: "Filtro Especial",
                }
            },
            pageable: {
                messages: {
                    display: "{0} - {1} de {2} Registros",
                    empty: "No existen datos",
                    page: "Página",
                    of: "de {0}",
                    itemsPerPage: "Páginas",
                    first: "Primero",
                    previous: "Anterior",
                    next: "Siguiente",
                    last: "Último",
                    refresh: "Refrescar"
                }
            }, dataBound: function (e) {
                $("#loading").fadeOut(200);
                setTimeout(
                    function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    }
                    , 100)
            }
        }
        // fin tabla de pedidos


        PedidoService.Pedidos().tipo_pedidos(function (tipo_pedidos) {

            $("#gridPedidos .k-filter-row").hide();
            $scope.tipo_pedidos = tipo_pedidos
        })
        PedidoService.Pedidos().clientes(function (lista_Clientes) {

            $scope.lista_clientes = lista_Clientes
        })
        PedidoService.Pedidos().vendedores(function (lista_vendedores) {

            $scope.lista_vendedores = lista_vendedores
        })



    }
    ini()
    $scope.buscar_estados = function () {

        PedidoService.Pedidos().estado_pedidos({ "tipo_pedido": $scope.tipo_pedido_elegido }, function (estado_pedidos) {

            $scope.estado_pedidos = estado_pedidos
        })
    }

    $scope.buscar_pedidos = function () {
        $scope.regs = PedidoService.Pedidos().query({
            "idEstado": $scope.estado_elegido, "idTipo": $scope.tipo_pedido_elegido, "idVendedor": $scope.vendedor_elegido,
            "idUsuario": $scope.cliente_elegido
        }, function (lista_pedidos) {
            $scope.registrations.read();
        })
    }

    $scope.mostrarRechazarPedido = function(dataItem){
        $scope.idPedidoMostrando = dataItem.id
        $scope.razonRechazo = dataItem.razonRechazo
        $scope.estadoIdPedido = dataItem.idEstado
        $("#popupMotivoRechazo").modal('toggle')
    }

    $scope.aceptar_pedido = function(id_pedido){
        $scope.responder_pedido(id_pedido, 1, '');
    }

    $scope.rechazar_pedido = function(){
        console.log()
        $scope.responder_pedido($scope.idPedidoMostrando, 0, $scope.razonRechazo)
        $("#popupMotivoRechazo").modal('toggle')
    }

    $scope.responder_pedido = function (id_pedido, respuesta, razonRechazo) {

        PedidoService.Pedidos().responder_pedido({ "id": id_pedido }, { "aprobado": respuesta, "razonRechazo": razonRechazo}, function (estado_pedidos) {
            $scope.buscar_pedidos();
        })

    }

    $scope.showOrder = function(dataItem){
        $scope.currentOrder = dataItem;
        $("#popupDetailOrder").modal('toggle')
    }

    $scope.buscar_pedidos();
}])