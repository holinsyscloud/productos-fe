HISProfile.factory('RankingService', ['$resource', '$q', '$http', 'connStringSVC', '$cookieStore', function ($resource, $q, $http, connStringSVC, $cookieStore) {

    return{
    Ranking: function(){
      return $resource(connStringSVC.urlBase()  + '/Ranking/:id', { id: '@_id' }, {
        query:   { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())} }
      });
    }
  }
    

}])