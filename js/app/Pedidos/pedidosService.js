HISProfile.factory('PedidoService', ['$resource', '$q', '$http', 'connStringSVC', '$cookieStore', function ($resource, $q, $http, connStringSVC, $cookieStore) {


    return {
        Pedidos: function () {
            return $resource(connStringSVC.urlBase() + '/PedidoWeb/:id', { id: '@_id' }, {
                save: { method: 'POST', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                query: { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                get: { method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                update: { method: 'PUT', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                delete: { method: 'DELETE', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                tipo_pedidos: { url: connStringSVC.urlBase() + '/TipoPedidos/', isArray: true, method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                estado_pedidos: { url: connStringSVC.urlBase() + '/EstadoPedidos/', isArray: true, method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                clientes: { url: connStringSVC.urlBase() + '/Clientes/', isArray: true, method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                updateClient: { url: connStringSVC.urlBase() + '/Cliente/', isArray: true, method: 'PUT', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                vendedores: { url: connStringSVC.urlBase() + '/Vendedores/', isArray: true, method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                responder_pedido: { url: connStringSVC.urlBase() + '/AprobarPedido/:id', method: 'PUT', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } }
            });
        },
        PedidosConfDesp: function () {
            return $resource(connStringSVC.urlBase() + '/PedidoConfDesp/:id', { id: '@_id' }, {
                query: { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
                responder_pedido: { method: 'PUT', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken()) } },
               
            });
        },
    }

}])