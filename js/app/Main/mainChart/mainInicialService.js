HISMainInicial.factory('MainService',['$resource','$q','$http','connStringSVC','$cookieStore',function($resource,$q,$http,connStringSVC,$cookieStore){


return{


  procesarDocumentoPolvo:function(){    
    return $resource(connStringSVC.urlBase() + '/documentosProcesar/:id', { id: '@_id' }, {
         save:    { method: 'POST', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  } },      
      
      });
      
    
  },

  procesarDocumentoGases:function(){    
    return $resource(connStringSVC.urlBase() + '/documentosProcesarGases/:id', { id: '@_id' }, {
         save:    { method: 'POST', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  } },      
      
      });      
    
  },
  procesarDocumentoAire:function(){    
    return $resource(connStringSVC.urlBase() + '/documentosProcesarAire/:id', { id: '@_id' }, {
         save:    { method: 'POST', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  } },      
      
      });      
    
  },
  areaBarrido:function(){
    return $resource(connStringSVC.urlBase() + '/areaBarrido/:id', { id: '@_id' }, {
         save:    { method: 'POST', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  } },
         query:   { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  } },
         get:     { method: 'GET', isArray: true, headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  }},
         update:  { method: 'PUT' , headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  }},
         delete:  { method: 'DELETE' , headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  }},
         filter:   {url: connStringSVC.urlBase() + '/AreaBarridoFilter/' , method: 'GET', headers: { 'Authorization': "Basic " + Base64.encode(connStringSVC.getToken())  } },
      });
      
    
  },
  nombreAnalisis:function(tipo, zona){

		var ano = ((new Date().getFullYear()).toString()).substr(2,3);
	    var dia= new Date().getDate()
	    var mes= new Date().getMonth()+1
	    
	    var hora=new Date().getHours()
	    var min=new Date().getMinutes()
	    var seg=new Date().getSeconds()

	    if(mes<10) { mes= "0"+ mes }
	    if(hora<10){ hora= "0"+ hora}
	    if(min<10) { min= "0"+ min}
	    if(seg<10) { seg= "0"+ seg}	  
	    if(dia<10) { dia= "0"+ dia}	  

	    var aleatorio= "Analisis ";   
		if (tipo) aleatorio =  tipo;
		if (zona) aleatorio = aleatorio + " (" + zona + ") ";
	    
		aleatorio = aleatorio  +dia+"/"+mes+"/" +ano;
	    return aleatorio
	}

}

}])